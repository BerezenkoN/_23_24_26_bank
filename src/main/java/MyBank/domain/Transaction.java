package MyBank.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * Created by user on 09.01.17.
 */
@JsonAutoDetect
public class Transaction {

    private long id;
    private BigDecimal amount;
    private Timestamp date;
    private String operationType;
    private long accountNumber;
    private long userId;

    public Transaction() {
        this.date = new Timestamp(System.currentTimeMillis());
    }

    public long getUserId() {
        return userId;
    }

    public Transaction(long id, BigDecimal amount, Timestamp date, String operationType, long accountNumber, long userId) {
        this.id = id;
        this.amount = amount;
        this.date = date;
        this.operationType = operationType;
        this.accountNumber = accountNumber;
        this.userId = userId;

    }

    public long getId() {
        return id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Timestamp getDate() {
        return date;
    }

    public String getOperationType() {
        return operationType;
    }

    public long getAccountNumber() {
        return accountNumber;
    }

    @Override
    public String toString() {

        return "Transaction{" +
                "id=" + id +
                ", amount=" + amount +
                ", date=" + date +
                ", operationType='" + operationType + '\'' +
                ", accountNumber=" + accountNumber +
                '}';
    }
}

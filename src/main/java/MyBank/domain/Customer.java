package MyBank.domain;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by user on 09.01.17.
 */

    @JsonAutoDetect
    public class Customer {
        @JsonIgnore
        private long id;

        private String firstName;

        private String lastName;
        @JsonIgnore
        private Date birthDate;

        private String strBirthDate;

        private Long userId;


        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        @JsonProperty("birthDate")
        public void setStrBirthDate(String strBirthDate) {
            this.strBirthDate = strBirthDate;
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date date = null;
            try {
                date = format.parse(strBirthDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            this.birthDate = new Date(date.getTime());
        }

        public Long getUserId() {
            return userId;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public void setPassport(String passport) {
            this.passport = passport;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        private String address;

        private String city;

        private String passport;

        private String phone;

        public Customer() {

        }



        public Customer(long id, String firstName, String lastName, Date birthDate, String address, String city, String passport, String phone, Long userId) {
            this.id = id;
            this.firstName = firstName;
            this.lastName = lastName;
            this.birthDate = birthDate;
            this.address = address;
            this.city = city;
            this.passport = passport;
            this.phone = phone;
            this.userId = userId;
        }

        public long getId() {
            return id;
        }

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public Date getBirthDate() {
            return birthDate;
        }

        public String getAddress() {
            return address;
        }

        public String getCity() {
            return city;
        }

        public String getPassport() {
            return passport;
        }

        public String getPhone() {
            return phone;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        @Override

        public String toString() {
            return "Customer{" +
                    "id=" + id +
                    ", firstName='" + firstName + '\'' +
                    ", lastName='" + lastName + '\'' +
                    ", birthDate=" + birthDate +
                    ", address='" + address + '\'' +
                    ", city='" + city + '\'' +
                    ", passport='" + passport + '\'' +
                    ", phone='" + phone + '\'' +

                    '}';
    }
}

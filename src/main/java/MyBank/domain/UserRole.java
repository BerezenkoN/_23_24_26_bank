package MyBank.domain;

/**
 * Created by user on 10.01.17.
 */
public enum UserRole {
    ADMIN,
    CUSTOMER;
}

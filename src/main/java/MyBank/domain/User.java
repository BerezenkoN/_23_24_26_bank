package MyBank.domain;


import com.fasterxml.jackson.annotation.JsonAutoDetect;

/**
 * Created by user on 09.01.17.
 */
@JsonAutoDetect
public class User {
    private Long id;
    private String login;
    private String password;
    private UserRole role;
    private String name;
    private Long customer;

    public User() {}

    public User(Long id, String login, String password, String name, UserRole role, Long customer) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.role = role;
        this.name = name;
        this.customer = customer;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public UserRole getRole() {
        return role;
    }

    public Long getId() {
        return id;
    }

    public Long getCustomer() {
        return customer;
    }

    public String getName() {
        return name;
    }
}
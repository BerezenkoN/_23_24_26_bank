package MyBank.dao;

import MyBank.domain.User;
import MyBank.domain.UserRole;
import org.apache.commons.codec.digest.DigestUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 10.01.17.
 */
public class UserDaoImpl implements UserDao {

    private static Connection connection = ConnectionUtill.getInstance();

    @Override
    public Long save(User user) {
        if (connection == null) return -1l;
        try (PreparedStatement statement = connection.prepareStatement(
                        "INSERT INTO " +
                                "users(login, password, name, userrole) " +
                                "VALUES (?, ?, ?, ?) " +
                                "RETURNING id;");
        ){
            statement.setString(1, user.getLogin());
            statement.setString(2, DigestUtils.md5Hex(user.getPassword()));
            statement.setString(3, user.getName());
            statement.setString(4, user.getRole().toString());
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            Long result = resultSet.getLong("id");
            resultSet.close();
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1l;
    }

    @Override
    public User get(Long id) {
        if (connection == null) return null;
        try (
                PreparedStatement statement = connection.prepareStatement("SELECT * FROM users WHERE id = ?;")
        ) {
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            User user = new User(
                    resultSet.getLong("id"),
                    resultSet.getString("login"),
                    resultSet.getString("password"),
                    resultSet.getString("name"),
                    UserRole.valueOf(resultSet.getString("userrole")),
                    null
                    );
            resultSet.close();
            return user;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int update(User user) {
        if (connection == null) return -1;
        try (PreparedStatement statement = connection.prepareStatement(
                "UPDATE users SET " +
                        "login = ?, " +
                        "password = ?, " +
                        "name = ?, " +
                        "userrole = ?;"
        )) {
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getName());
            statement.setString(4, user.getRole().toString());
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public int delete(Long id) {
        if (connection == null) return -1;
        try (PreparedStatement statement = connection.prepareStatement(
                "DELETE FROM users WHERE id = ?;"
        )) {
            statement.setLong(1, id);
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public List<User> list() {
        if (connection == null) return null;
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM users;");
            List<User> userList = new ArrayList<>();
            while (resultSet.next()) {
                User user = new User(
                        resultSet.getLong("id"),
                        resultSet.getString("login"),
                        resultSet.getString("password"),
                        resultSet.getString("name"),
                        UserRole.valueOf(resultSet.getString("userrole")),
                        null
                );
                userList.add(user);
            }
            resultSet.close();
            return userList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {
    }

}

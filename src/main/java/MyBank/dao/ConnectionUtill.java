package MyBank.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by user on 16.01.2017.
 */
public class ConnectionUtill {


        private static Connection connection;

        private ConnectionUtill() {};

        public static Connection getInstance() {
            if (connection == null) {
                try {
                    Class.forName("org.postgresql.Driver");
                    connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/bank", "postgres", "14881488");
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return connection;
        }

    }

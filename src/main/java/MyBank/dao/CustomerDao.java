package MyBank.dao;


import MyBank.domain.Customer;

import java.util.List;

/**
 * Created by user on 09.01.17.
 */
public interface CustomerDao {

    Customer get(Long Id);

    Long save(Customer customer);

    void update(Customer customer);

    int delete(Long id);

    List<Customer> list();

}

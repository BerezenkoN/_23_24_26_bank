package MyBank.dao;

import MyBank.domain.Transaction;

import java.util.List;

/**
 * Created by user on 08.01.17.
 */
public interface TransactionDao {

    Transaction get(Long Id);

    Long save(Transaction Transaction);

    void update(Transaction Transaction);

    int delete(Long id);

    List<Transaction> list();

    int deleteByAccount(Long accountId);

}

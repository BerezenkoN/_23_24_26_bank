package MyBank.dao;
import MyBank.domain.User;

import java.util.List;

/**
 * Created by user on 10.01.17.
 */
public interface UserDao {

    Long save(User user);

    User get(Long id);

    int update(User user);

    int delete(Long id);

    List<User> list();

}

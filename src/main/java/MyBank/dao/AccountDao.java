package MyBank.dao;

import MyBank.domain.Account;

import java.util.List;
/**
 * Created by user on 08.01.17.
 */
public interface AccountDao {

    Account get(Long accountNumber);

    Long save(Account account);

    void update(Account account);

    int delete(Long accountNumber);

    List<Account> list();

    int deleteByCustomer(Long customerId);
}

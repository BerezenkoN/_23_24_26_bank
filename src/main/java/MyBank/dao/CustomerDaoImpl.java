package MyBank.dao;


import MyBank.domain.Customer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 09.01.17.
 */
public class CustomerDaoImpl implements CustomerDao {

    private static Connection connection = ConnectionUtill.getInstance();

    @Override
    public Customer get(Long id) {
        if (connection == null) return null;
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM customers WHERE id = ?;");
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            Customer customer = new Customer(
                    resultSet.getLong("id"),
                    resultSet.getString("firstname"),
                    resultSet.getString("lastname"),
                    resultSet.getDate("birthdate"),
                    resultSet.getString("address"),
                    resultSet.getString("city"),
                    resultSet.getString("passport"),
                    resultSet.getString("phone"),
                    resultSet.getLong("user_id"));
            resultSet.close();
            statement.close();
            return customer;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Long save(Customer customer) {
        if (connection == null) return null;
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO customers (firstname, lastname, birthdate, " +
                            "address, city, passport, phone, user_id) " +
                            "VALUES (?, ?, ?, ?, ?, ?, ?, ?) " +
                            "RETURNING id;");
            statement.setString(1, customer.getFirstName());
            statement.setString(2, customer.getLastName());
            statement.setDate(3, customer.getBirthDate());
            statement.setString(4, customer.getAddress());
            statement.setString(5, customer.getCity());
            statement.setString(6, customer.getPassport());
            statement.setString(7, customer.getPhone());
            if (customer.getUserId() == null || customer.getUserId() == 0l)
                statement.setNull(8, java.sql.Types.INTEGER);
            else
                statement.setLong(8, customer.getUserId());
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            Long id = resultSet.getLong("id");
            resultSet.close();
            statement.close();
            return id;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void update(Customer customer) {
        if (connection == null) return;
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE customers SET " +
                            "firstname = ?, " +
                            "lastname = ?, " +
                            "birthdate = ?, " +
                            "address = ?, " +
                            "city = ?," +
                            "passport = ?, " +
                            "phone = ?, " +
                            "user_id = ?" +
                            "WHERE id = ?");
            statement.setString(1, customer.getFirstName());
            statement.setString(2, customer.getLastName());
            statement.setDate(3, customer.getBirthDate());
            statement.setString(4, customer.getAddress());
            statement.setString(5, customer.getCity());
            statement.setString(6, customer.getPassport());
            statement.setString(7, customer.getPhone());
            if (customer.getUserId() == null)
                statement.setNull(8, java.sql.Types.INTEGER);
            else
                statement.setLong(8, customer.getUserId());
            statement.setLong(9, customer.getId());
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int delete(Long id) {
        if (connection == null) return 0;
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM customers WHERE id = ?;");
            statement.setLong(1, id);
            int result = statement.executeUpdate();
            statement.close();
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public List<Customer> list() {
        if (connection == null) return null;
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM customers;");
            List<Customer> customers = new ArrayList<>();
            while (resultSet.next()) {
                Customer customer = new Customer(
                        resultSet.getLong("id"),
                        resultSet.getString("firstname"),
                        resultSet.getString("lastname"),
                        resultSet.getDate("birthdate"),
                        resultSet.getString("address"),
                        resultSet.getString("city"),
                        resultSet.getString("passport"),
                        resultSet.getString("phone"),
                        resultSet.getLong("user_id"));
                customers.add(customer);
            }
            resultSet.close();
            statement.close();
            return customers;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


}

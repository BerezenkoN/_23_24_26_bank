package MyBank.dao;


import MyBank.domain.Account;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 08.01.17.
 */
public class AccountDaoImpl implements AccountDao {

    private static Connection connection = ConnectionUtill.getInstance();

    @Override
    public Account get(Long accountNumber) {
        if (connection == null) return null;
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT a.*, c.user_id AS user_id FROM accounts AS a LEFT JOIN customers AS c ON a.customerid = c.id WHERE a.accountnumber= ?;");
            statement.setLong(1, accountNumber);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            Account account = new Account(
                    resultSet.getLong("accountnumber"),
                    resultSet.getBigDecimal("balance"),
                    resultSet.getTimestamp("creationdate"),
                    resultSet.getString("currency"),
                    resultSet.getBoolean("blocked"),
                    resultSet.getLong("customerid"),
                    resultSet.getLong("user_id")
            );
            resultSet.close();
            statement.close();
            return account;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Long save(Account account) {
        if (connection == null) return null;
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO accounts (balance, creationdate, currency, blocked, customerid) " +
                            "VALUES (?, ?, ?, ?, ?) " +
                            "RETURNING accountnumber;");
            statement.setBigDecimal(1, account.getBalance());
            statement.setTimestamp(2, account.getCreationDate());
            statement.setString(3, account.getCurrency());
            statement.setBoolean(4, account.isBlocked());
            statement.setLong(5, account.getCustomerId());
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            Long result = resultSet.getLong("accountnumber");
            resultSet.close();
            statement.close();
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void update(Account account) {
        if (connection == null) return;
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE accounts SET " +
                            "balance = ?, " +
                            "creationdate = ?, " +
                            "currency = ?, " +
                            "blocked = ?, " +
                            "customerid = ? " +
                            "WHERE accountnumber = ?;");
            statement.setBigDecimal(1, account.getBalance());
            statement.setTimestamp(2, account.getCreationDate());
            statement.setString(3, account.getCurrency());
            statement.setBoolean(4, account.isBlocked());
            statement.setLong(5, account.getCustomerId());
            statement.setLong(6, account.getAccountNumber());
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int delete(Long accountNumber) {
        if (connection == null) return 0;
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM accounts WHERE accountnumber = ?;");
            statement.setLong(1, accountNumber);
            int result = statement.executeUpdate();
            statement.close();
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public List<Account> list() {
        if (connection == null) return null;
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT a.*, c.user_id AS user_id FROM accounts AS a LEFT JOIN customers AS c ON a.customerid = c.id;");
            List<Account> accounts = new ArrayList<>();
            while (resultSet.next()) {
                Account account = new Account(
                        resultSet.getLong("accountnumber"),
                        resultSet.getBigDecimal("balance"),
                        resultSet.getTimestamp("creationdate"),
                        resultSet.getString("currency"),
                        resultSet.getBoolean("blocked"),
                        resultSet.getLong("customerid"),
                        resultSet.getLong("user_id")
                );
                accounts.add(account);
            }
            resultSet.close();
            statement.close();
            return accounts;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int deleteByCustomer(Long customerId) {
        if (connection == null) return 0;
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM accounts WHERE customerid = ?;");
            statement.setLong(1, customerId);
            int result = statement.executeUpdate();
            statement.close();
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
}

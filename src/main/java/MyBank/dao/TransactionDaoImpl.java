package MyBank.dao;

import MyBank.domain.Transaction;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 08.01.17.
 */
public class TransactionDaoImpl implements TransactionDao {

    private static Connection connection = ConnectionUtill.getInstance();

    @Override
    public Transaction get(Long id) {
        if (connection == null) return null;
        try {
            PreparedStatement statement = connection.prepareStatement("" +
                    "SELECT t.*, c.user_id AS user_id " +
                        "FROM transactions AS t " +
                        "LEFT JOIN accounts AS a on t.accountnumber = a.accountnumber " +
                        "LEFT JOIN customers AS c ON a.customerid = c.id " +
                        "WHERE t.id = ?;");
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            Transaction transaction = new Transaction(
                    resultSet.getLong("id"),
                    resultSet.getBigDecimal("amount"),
                    resultSet.getTimestamp("date"),
                    resultSet.getString("operationtype"),
                    resultSet.getLong("accountnumber"),
                    resultSet.getLong("user_id"));
            resultSet.close();
            statement.close();
            return transaction;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Long save(Transaction transaction) {
        if (connection == null) return null;
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO transactions (amount, date, operationtype, accountnumber) " +
                            "VALUES (?, ?, ?, ?) " +
                            "RETURNING id;"
            );
            statement.setBigDecimal(1, transaction.getAmount());
            statement.setTimestamp(2, transaction.getDate());
            statement.setString(3, transaction.getOperationType());
            statement.setLong(4, transaction.getAccountNumber());
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            Long result = resultSet.getLong("id");
            resultSet.close();
            statement.close();
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void update(Transaction transaction) {
        if (connection == null) return;
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE transactions SET " +
                            "amount = ?, " +
                            "date = ?, " +
                            "operationtype = ?, " +
                            "accountnumber = ? " +
                            "WHERE id = ?;");
            statement.setBigDecimal(1, transaction.getAmount());
            statement.setTimestamp(2, transaction.getDate());
            statement.setString(3, transaction.getOperationType());
            statement.setLong(4, transaction.getAccountNumber());
            statement.setLong(5, transaction.getId());
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int delete(Long id) {
        if (connection == null) return 0;
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "DELETE FROM transactions WHERE  id = ?;");
            statement.setLong(1, id);
            int result = statement.executeUpdate();
            statement.close();
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public List<Transaction> list() {
        if (connection == null) return null;
        try {
            Statement statement = connection.createStatement();
            List<Transaction> transactionList = new ArrayList<>();
            ResultSet resultSet = statement.executeQuery(
                    "SELECT t.*, c.user_id AS user_id " +
                    "FROM transactions AS t " +
                    "LEFT JOIN accounts AS a on t.accountnumber = a.accountnumber " +
                    "LEFT JOIN customers AS c ON a.customerid = c.id ;");
            while (resultSet.next()) {
                Transaction transaction = new Transaction(
                        resultSet.getLong("id"),
                        resultSet.getBigDecimal("amount"),
                        resultSet.getTimestamp("date"),
                        resultSet.getString("operationtype"),
                        resultSet.getLong("accountnumber"),
                        resultSet.getLong("user_id"));
                transactionList.add(transaction);
            }
            resultSet.close();
            statement.close();
            return transactionList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int deleteByAccount(Long accountId) {
        if (connection == null) return 0;
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "DELETE FROM transactions WHERE accountnumber = ?;");
            statement.setLong(1, accountId);
            int result = statement.executeUpdate();
            statement.close();
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

}

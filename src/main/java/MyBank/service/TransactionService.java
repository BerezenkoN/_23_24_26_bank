package MyBank.service;

import MyBank.dao.TransactionDaoImpl;
import MyBank.dao.TransactionDao;
import MyBank.domain.Transaction;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by user on 10.01.17.
 */
public class TransactionService {

    private static TransactionDao transactionDao = new TransactionDaoImpl();

    public static List<Transaction> list() {
        return transactionDao.list();
    }

    public static List<Transaction> listByAccount(long accountNumber) {
        return transactionDao.list().stream()
                .filter(transaction -> (transaction.getAccountNumber() == accountNumber))
                .collect(Collectors.toList());
    }

    public static int deleteByAccount(Long accountNumber) {
        return transactionDao.deleteByAccount(accountNumber);
    }

    public static Long save(Transaction transaction) {
        switch (transaction.getOperationType()) {
            case "PUT": AccountService.increaseBalance(transaction.getAccountNumber(), transaction.getAmount()); break;
            case "WTH": AccountService.decreaseBalance(transaction.getAccountNumber(), transaction.getAmount()); break;
        }
        return transactionDao.save(transaction);
    }

    public static Transaction get(long id) {
        return transactionDao.get(id);
    }

    public static int delete(Long id) {
        return transactionDao.delete(id);
    }


}



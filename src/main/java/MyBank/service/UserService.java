package MyBank.service;

import org.apache.commons.codec.digest.DigestUtils;
import MyBank.dao.UserDaoImpl;
import MyBank.dao.UserDao;
import MyBank.domain.User;

import java.util.List;

/**
 * Created by user on 10.01.17.
 */
public class UserService {

    private static UserDao userDao = new UserDaoImpl();

    public static User get(Long id) {
        return userDao.get(id);
    }

    public static boolean isExists(String login) {
        return userDao.list()
                .stream()
                .filter(user -> user.getLogin().equals(login)).count() > 0;
    }

    public static Long save(User user) {
        return userDao.save(user);
    }

    public static User get(String login, String password) {
        return userDao.list()
                .stream()
                .filter(user -> (user.getLogin().equals(login) && user.getPassword().equals(DigestUtils.md5Hex(password))))
                .findFirst().get();
    }

    public static List<User> list() {
        return userDao.list();
    }

    public static int delete(Long id) {
        CustomerService.clearUserId(id);
        return userDao.delete(id);
    }

    public static boolean isValid(String login, String password) {
        return userDao.list()
                .stream()
                .filter(user -> (user.getLogin().equals(login) && user.getPassword().equals(DigestUtils.md5Hex(password))))
                .count() > 0;
    }


    public static void main(String[] args) {
        delete(25l);
    }

}

package MyBank.controller;

import MyBank.domain.UserRole;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by user on 10.01.17.
 */
public class AdminCustomerFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpSession session = req.getSession();
        UserRole role  = (UserRole)session.getAttribute("role");
        if (role == UserRole.ADMIN) {
            filterChain.doFilter(req, servletResponse);
        }
        else {
            HttpServletResponse resp = (HttpServletResponse) servletResponse;
            resp.sendRedirect("/index");
        }
    }

    @Override
    public void destroy() {

    }

}

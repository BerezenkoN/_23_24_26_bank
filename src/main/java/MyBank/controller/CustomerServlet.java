package MyBank.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import MyBank.domain.Customer;
import MyBank.service.AccountService;
import MyBank.service.CustomerService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by user on 10.01.17.
 */
public class CustomerServlet extends HttpServlet {

    private Pattern customerDetailsPattern = Pattern.compile("/customers/(\\d+)");
    private Pattern customerListPattern = Pattern.compile("/customers");
    private Pattern customerNewPattern = Pattern.compile("/customers/new");
    private Pattern customerDeletePattern = Pattern.compile("/customers/delete");
    private Pattern customerAddPattern = Pattern.compile("/customers/add");


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String requestURI = req.getRequestURI();

        if (customerListPattern.matcher(requestURI).matches()) {
            req.setAttribute("customerList", CustomerService.list());
            req.getRequestDispatcher("/customers.jsp").forward(req, resp);
            return;
        }

        if (customerNewPattern.matcher(requestURI).matches()) {
            req.setAttribute("customerList", CustomerService.list());
            req.getRequestDispatcher("/customer_form.jsp").forward(req, resp);
            return;
        }

        if (customerDetailsPattern.matcher(requestURI).matches()) {
            Matcher m = customerDetailsPattern.matcher(requestURI);
            m.find();
            Long customerId = Long.parseLong(m.group(1));
            Customer customer = CustomerService.get(customerId);
            req.setAttribute("firstname", customer.getFirstName());
            req.setAttribute("lastname", customer.getLastName());
            req.setAttribute("accountList", AccountService.listByCustomer(customerId));
            req.getRequestDispatcher("/customer.jsp").forward(req, resp);
            return;
        }

        resp.sendError(404);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(req.getInputStream()));
        String json = "";
        if (reader != null) {
            json = reader.readLine();
        }
        ObjectMapper mapper = new ObjectMapper();

        String requestURI = req.getRequestURI();

        if (customerDeletePattern.matcher(requestURI).matches()) {
            Long id = mapper.readValue(json, long.class);
            Long res = CustomerService.delete(id) > 0? id : 0;
            resp.setContentType("application/json");
            mapper.writeValue(resp.getOutputStream(), res);
            return;
        }

        if (customerAddPattern.matcher(requestURI).matches()) {
            Customer customer = mapper.readValue(json, Customer.class);
            Long id = CustomerService.save(customer);
            resp.setContentType("application/json");
            mapper.writeValue(resp.getOutputStream(), id);
            return;
        }

    }

}

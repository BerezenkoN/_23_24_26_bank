package MyBank.controller;

import MyBank.domain.User;
import MyBank.domain.UserRole;
import MyBank.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * Created by user on 10.01.17.
 */
public class RegisterServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("register.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, String[]> params = req.getParameterMap();
        User user = new User(
                0l,
                params.get("login")[0],
                params.get("password")[0],
                params.get("name")[0],
                UserRole.CUSTOMER,
                null);
        if (UserService.isExists(user.getLogin()))
            req.setAttribute("result", "ERROR: User with same login already exists");
        else if (UserService.save(user) > 0)
            req.setAttribute("result", "User created successfully. You can Sign In now.");
        else
            req.setAttribute("result", "Error while creating user");

        req.getRequestDispatcher("/login.jsp").forward(req, resp);
    }
}

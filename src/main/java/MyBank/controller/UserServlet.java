package MyBank.controller;

import MyBank.domain.Customer;
import MyBank.domain.User;
import MyBank.service.CustomerService;
import MyBank.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Pattern;

/**
 * Created by user on 10.01.17.
 */
public class UserServlet extends HttpServlet {

    Pattern userListPattern = Pattern.compile("/users");
    Pattern userNewPattern = Pattern.compile("/users/new");
    Pattern userAddPattern = Pattern.compile("/users/add");
    Pattern userDeletePattern = Pattern.compile("/users/delete");

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        String requestURI = req.getRequestURI();
        if (userListPattern.matcher(requestURI).matches()) {
            req.setAttribute("userList", UserService.list());
            req.getRequestDispatcher("/users.jsp").forward(req, resp);
            return;
        }

        if (userNewPattern.matcher(requestURI).matches()) {
            req.setAttribute("userList", UserService.list());
            req.setAttribute("customerList", CustomerService.list());
            req.getRequestDispatcher("/user_form.jsp").forward(req, resp);
            return;
        }

        resp.sendError(404);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String requestURI = req.getRequestURI();
        ObjectMapper mapper = new ObjectMapper();
        String json = "";
        BufferedReader reader = new BufferedReader(new InputStreamReader(req.getInputStream()));
        if (reader != null) {
            json = reader.readLine();
        }
        if (userAddPattern.matcher(requestURI).matches()) {
            User user = mapper.readValue(json, User.class);
            Long id = UserService.save(user);
            if (id > 0) {
                Long customerId = user.getCustomer();
                Customer customer = CustomerService.get(customerId);
                customer.setUserId(id);
                CustomerService.update(customer);
                User responseUser = UserService.get(id);
                resp.setContentType("application/json");
                mapper.writeValue(resp.getOutputStream(), responseUser);
            }
            return;
        }

        if (userDeletePattern.matcher(requestURI).matches()) {
            Long id = mapper.readValue(json, long.class);
            Long res = UserService.delete(id) > 0 ? id : 0;
            resp.setContentType("application/json");
            mapper.writeValue(resp.getOutputStream(), res);
            return;
        }
    }
}

package MyBank.controller;

import MyBank.domain.User;
import MyBank.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by user on 10.01.17.
 */
public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getParameter("action");
        switch (action) {
            case "login":
                String login = req.getParameter("login");
                String password = req.getParameter("password");
                if (UserService.isValid(login, password)) {
                    HttpSession session = req.getSession();
                    User user = UserService.get(login, password);
                    session.setAttribute("login", user.getLogin());
                    session.setAttribute("password", password);
                    session.setAttribute("name", user.getName());
                    session.setAttribute("role", user.getRole());
                    session.setAttribute("userId", user.getId());
                    resp.sendRedirect("/index");
                } else {
                    req.setAttribute("result", "ERROR: incorrect login/password");
                    req.getRequestDispatcher("/login.jsp").forward(req, resp);
                }
                break;
            case "register":
                resp.sendRedirect("/register");
                break;
        }
    }
}

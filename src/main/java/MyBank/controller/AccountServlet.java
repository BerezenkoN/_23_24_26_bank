package MyBank.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import MyBank.domain.Account;
import MyBank.service.AccountService;
import MyBank.service.CustomerService;
import MyBank.service.TransactionService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by user on 10.01.17.
 */
public class AccountServlet extends HttpServlet {

    private Pattern accountDetailsPattern = Pattern.compile("/accounts/(\\d+)");
    private Pattern accountListPattern = Pattern.compile("/accounts");
    private Pattern accountNewPattern = Pattern.compile("/accounts/new");
    private Pattern accountAddPattern = Pattern.compile("/accounts/add");
    private Pattern accountDeletePattern = Pattern.compile("/accounts/delete");

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String requestURI = req.getRequestURI();

        if (accountListPattern.matcher(requestURI).matches()) {
            req.setAttribute("accountList", AccountService.list());
            req.getRequestDispatcher("/accounts.jsp").forward(req, resp);
            return;
        }

        if (accountDetailsPattern.matcher(requestURI).matches()) {
            Matcher m = accountDetailsPattern.matcher(requestURI);
            m.find();
            Long accountNumber = Long.parseLong(m.group(1));
            req.setAttribute("accountNumber", accountNumber);
            req.setAttribute("transactionList", TransactionService.listByAccount(accountNumber));
            req.getRequestDispatcher("/account.jsp").forward(req, resp);
            return;
        }

        if (accountNewPattern.matcher(requestURI).matches()) {
            req.setAttribute("accountList", AccountService.list());
            req.setAttribute("customerList", CustomerService.list());
            req.getRequestDispatcher("/account_form.jsp").forward(req, resp);
            return;
        }

        resp.sendError(404);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(req.getInputStream()));

        String requestURI = req.getRequestURI();

        String json = "";
        if (reader != null) {
            json = reader.readLine();
        }
        ObjectMapper mapper = new ObjectMapper();

        if (accountAddPattern.matcher(requestURI).matches()) {
            Account account = mapper.readValue(json, Account.class);
            Long id = AccountService.save(account);
            Account respAccount = AccountService.get(id);
            Map<String, String> result = new HashMap<>();
            result.put("accountNumber", String.valueOf(respAccount.getAccountNumber()));
            result.put("creationDate", respAccount.getCreationDate().toString());
            resp.setContentType("application/json");
            mapper.writeValue(resp.getOutputStream(), result);
            return;
        }

        if (accountDeletePattern.matcher(requestURI).matches()) {
            Long id = mapper.readValue(json, long.class);
            Long res = AccountService.delete(id) > 0? id : 0;
            resp.setContentType("application/json");
            mapper.writeValue(resp.getOutputStream(), res);
            return;
        }

    }
}

$('.login_submit').click(
    function() {
        if ($('#login').val() == '' ||
            $('#name').val() == '' ||
            $('#password').val() == '' ||
            $('#password_confirm' == ''))
            alert('You must fill all the fields')

        if ($('#password') != ('#password_confirm')) {
            alert('Password doesn\'t match with confirmation');
            $('#password').val('');
            $('#password_confirm').val('');
        }
    }
);

$('#transaction_list').on('click', '#transaction_delete', function() {
    $.ajax({
        url: "/transactions/delete",
        type: 'POST',
        dataType: 'json',
        data: $(this).parent().parent().attr("id"),
        contentType: 'application/json',
        mimeType: 'application/json',
        success: function(data) {
          if (data == 0) alert ("Error while deleting record");
          else {
                var id = '#'+data;
                $(id).remove();
            }
          },
        error: function(data) {
          alert("Error while deleting record")
        }

    });
});

$('#user_list').on('click', '#user_delete', function() {
    $.ajax({
        url: "/users/delete",
        type: 'POST',
        dataType: 'json',
        data: $(this).parent().parent().attr("id"),
        contentType: 'application/json',
        mimeType: 'application/json',
        success: function(data) {
          if (data == 0) alert ("Error while deleting record");
          else {
                var id = '#'+data;
                $(id).remove();
            }
          },
        error: function(data) {
          alert("Error while deleting record")
        }

    });
});

$('#customer_list').on('click', '#customer_delete', function() {
    $.ajax({
        url: "/customers/delete",
        type: 'POST',
        dataType: 'json',
        data: $(this).parent().parent().attr("id"),
        contentType: 'application/json',
        mimeType: 'application/json',
        success: function(data) {
          if (data == 0) alert ("Error while deleting record");
          else {
                var id = '#'+data;
                $(id).remove();
            }
          },
        error: function(data) {
          alert("Error while deleting record")
        }

    });
});

$('#account_list').on('click', '#account_delete', function() {
    $.ajax({
        url: "/accounts/delete",
        type: 'POST',
        dataType: 'json',
        data: $(this).parent().parent().attr("id"),
        contentType: 'application/json',
        mimeType: 'application/json',
        success: function(data) {
          if (data == 0) alert ("Error while deleting record");
          else {
                var id = '#'+data;
                $(id).remove();
            }
          },
        error: function(data) {
          alert("Error while deleting record")
        }

    });
});

$('#transaction_save').click(
    function() {
        var transaction = {
            amount: $('#amount').val(),
            accountNumber: $('#accountNumber').val(),
            operationType: $('#operationType').val()
        }
        $.ajax({
            url: '/transactions/add',
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify(transaction),
            contentType: 'application/json',
            mimeType: 'application/json',
            success: function(data){
                $('#transaction_list').last().append(
                    "<tr id=\"" + data.id + "\">" +
                        "<td>" + data.amount + "</td>" +
                        "<td>" + data.date + "</td>" +
                        "<td>" + data.operationType + "</td>" +
                        "<td>" + data.accountNumber + "</td>" +
                        "<td><div id=\"transaction_delete\"><img src=\"images/delete.png\" /></div></td>" +
                    "</tr>"
                );
                $('#transaction_form').trigger('reset');
            },
            error: function() {
                alert('Error while creating transaction');
            }
        });
    }
);

$('#account_save').click(
    function() {
        var account = {
            customerId : $('#customer').val(),
            balance : $('#balance').val(),
            currency : $('#currency').val(),
            blocked : $('#blocked').prop('checked')
        }
        $.ajax({
            url: '/accounts/add',
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify(account),
            contentType: 'application/json',
            mimeType: 'application/json',
            success: function(data){
                $('#account_list').last().append(
                "<tr id=\"" + data.accountNumber + "\">" +
                "<td><a href=\"/accounts/"+data.accountNumber+"\">"+data.accountNumber+"</a></td>"+
                "<td>"+account.balance+"</td>"+
                "<td>"+data.creationDate+"</td>"+
                "<td>"+account.currency+"</td>"+
                "<td>"+account.blocked+"</td>"+
                "<td><div id=\"account_delete\"><img src=\"/images/delete.png\" /></div></td>" +
                "</tr>"
                );
                $('#account_form').trigger('reset');
            },
            error: function() {
                alert('Error while creating account');
            }
        });
    }
);

$('#customer_save').click(function() {
        var customer = {
            firstName: $('#firstName').val(),
            lastName: $('#lastName').val(),
            birthDate: $('#birthdate').val(),
            address: $('#address').val(),
            city: $('#city').val(),
            passport: $('#passport').val(),
            phone: $('#phone').val()
            };
        $.ajax({
            url: "/customers/add",
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify(customer),
            contentType: 'application/json',
            mimeType: 'application/json',
            success: function(data) {
                $('#customer_list').last().append(
                "<tr id=\"" + data + "\">"+
                    "<td><a href=\"/customers/"+data+"\">"+customer.firstName+"</a></td>"+
                    "<td>"+customer.lastName+"</td>"+
                    "<td>"+customer.birthDate+"</td>"+
                    "<td>"+customer.address+"</td>"+
                    "<td>"+customer.city+"</td>"+
                    "<td>"+customer.passport+"</td>"+
                    "<td>"+customer.phone+"</td>"+
                    "<td><div id=\"customer_delete\"><img src=\"images/delete.png\" /></div></td>" +
                    "</tr>"
                );
                $('#customer_form').trigger('reset');
            },
            error: function(data,status,er) {
                    alert("Error while creating customer");
                }
        });
});

$('#user_save').click(
    function() {
        var user = {
            login: $('#username').val(),
            name: $('#name').val(),
            password: $('#password').val(),
            role: $('#userRole').val(),
            customer: $('#customer').val()
        }
        $.ajax({
            url: '/users/add',
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify(user),
            contentType: 'application/json',
            mimeType: 'application/json',
            success: function(data){
                $('#user_list').last().append(
                    "<tr id=\"" + data.id + "\">" +
                        "<td>" + data.login + "</td>" +
                        "<td>" + data.name + "</td>" +
                        "<td>" + data.role + "</td>" +
                        "<td>" + user.customer + "</td>" +
                        "<td><div id=\"transaction_delete\"><img src=\"images/delete.png\" /></div></td>" +
                    "</tr>"
                );
                $('#user_form').trigger('reset');
            },
            error: function() {
                alert('Error while creating user');
            }
        });
    }
);
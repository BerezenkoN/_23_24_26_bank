<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/css/style.css" />
    <title>Account</title>
</head>
<body>
    <div id="navigation_menu">
        <ul>
            <li id="back"><a href="/accounts"><img src="/images/back.png" /></a></li>
        </ul>
    </div>
    <div id="main_content">
        <c:set var="accountNumber" value="${accountNumber}" />
        <h1>Account ${accountNumber}</h1>
        <table>
            <tr>
                <th>Amount</th>
                <th>Date</th>
                <th>Operation type</th>
                <th>Account number</th>
            </tr>
            <c:forEach var="transaction" items="${transactionList}">
                <c:if test="${(sessionScope.role == 'ADMIN') || (sessionScope.userId == transaction.userId)}">
                    <tr>
                        <td>${transaction.amount}</td>
                        <td>${transaction.date}</td>
                        <td>${transaction.operationType}</td>
                        <td>${transaction.accountNumber}</td>
                    </tr>
                </c:if>
            </c:forEach>
        </table>
    </div>
</body>
</html>
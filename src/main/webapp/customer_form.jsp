<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/css/style.css"> </link>
    <title>Form</title>
</head>
<body>
    <div id="navigation_menu">
        <ul>
            <li><a id="current_page" href="/customers/">Customers</a></li>
            <li><a href="/accounts">Accounts</a></li>
            <li><a href="/transactions">Transactions</a></li>
            <c:if test="${sessionScope.role == 'ADMIN'}">
                <li><a href="/users">Users</a></li>
            </c:if>
            <li id="home"><a href="/index"><img src="/images/Home13.png" /></a></li>
        </ul>
    </div>
    <div id="main_content">
        <h1>Creating customer</h1>
        <form id="customer_form" action="#">
            First name<br />
            <input id="firstName" type="text" name="firstName" placeholder="First name"/><br />
            Last name<br />
            <input id="lastName" type="text" name="lastName" placeholder="Last name"/><br />
            Birthdate<br />
            <input id="birthDate" type="text" name="birthDate" placeholder="Birth date"/><br />
            Address<br />
            <input id="address" type="text" name="address" placeholder="Address" /><br />
            City<br />
            <input id="city" type="text" name="city" placeholder="City" /><br />
            Passport<br />
            <input id="passport" type="text" name="passport" placeholder="Passport" /><br />
            Phone<br />
            <input id="phone" type="text" name="phone" placeholder="Phone" /><br />
            <div id="customer_save">Save</div>
        </form>
        <table id="customer_list">
            <tr>
                <th>Firstname</th>
                <th>Lastname</th>
                <th>Birthdate</th>
                <th>Address</th>
                <th>City</th>
                <th>Passport</th>
                <th>Phone</th>
                <th></th>
            </tr>
            <c:forEach var="customer" items="${customerList}">
                <c:if test="${(sessionScope.role == 'ADMIN') || (sessionScope.userId == customer.userId)}">
                    <tr id="${customer.id}">
                        <td><a href="/customers/${customer.id}">${customer.firstName}</a></td>
                        <td>${customer.lastName}</td>
                        <td>${customer.birthDate}</td>
                        <td>${customer.address}</td>
                        <td>${customer.city}</td>
                        <td>${customer.passport}</td>
                        <td>${customer.phone}</td>
                        <td><div id="customer_delete"><img src="/images/delete.png" /></div></td>
                    </tr>
                </c:if>
            </c:forEach>
        </table>
    </div>
        <script type="text/javascript" src="/javascript/jquery.js"></script>
        <script type="text/javascript" src="/javascript/script.js"></script>
</body>
</html>
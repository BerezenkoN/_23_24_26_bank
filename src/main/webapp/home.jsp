<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/css/style.css" />
    <title>Home</title>
</head>
<body>
    <div id="navigation_menu">
        <ul>
            <li><a href="/customers">Customers</a></li>
            <li><a href="/accounts">Accounts</a></li>
            <li><a href="/transactions">Transactions</a></li>
            <c:if test="${sessionScope.role == 'ADMIN'}">
                <li><a href="/users">Users</a></li>
            </c:if>
        </ul>
    </div>
    <div id="main_content">
        <h1>Hello, ${sessionScope.name}! Welcome to the bank.</h1>
        <h1>You have ${sessionScope.role} privileges.</h1>
    </div>
</body>
</html>
<!DOCTYPE html>
<html>
  <head>
    <title>Register new user</title>
    <link rel="stylesheet" type="text/css" href="/css/style.css" />
  </head>
  <body>
    <form method="POST" action="#" id="register_form">
      <h1>Registration</h1>
      Login<br />
      <input type="text" name="login" placeholder="Login" /><br />
      Name<br />
      <input type="text" name="name" placeholder="Name" /><br />
      Password<br />
      <input type="password" name="password" placeholder="Password" /><br />
      Confirm password<br />
      <input type="password" name="password_confirm" placeholder="Password" /><br />
      <button type="submit" class="login_submit" >Create</button>
    </form>
    <script type="text/javascript" src="/javascript/jquery.js"></script>
    <script type="text/javascript" src="/javascript/script.js"></script>
  </body>
</html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/css/style.css" />
    <title>Customer</title>
</head>
<body>
    <div id="navigation_menu">
        <ul>
            <li id="back"><a href="/customers"><img src="/images/back.png" /></a></li>
        </ul>
    </div>
    <c:set var="firstname" value="${firstname}" />
    <c:set var="lastname" value="${lastname}" />
    <div id="main_content">
        <h1>${firstname} ${lastname}</h1>
        <table>
            <tr>
                <th>Account number</th>
                <th>Balance</th>
                <th>Creation date</th>
                <th>Currency</th>
                <th>Blocked</th>
            </tr>
            <c:forEach var="account" items="${accountList}">
                <c:if test="${(sessionScope.role == 'ADMIN') || (sessionScope.userId == account.userId)}">
                    <tr>
                        <td>${account.accountNumber}</td>
                        <td>${account.balance}</td>
                        <td>${account.creationDate}</td>
                        <td>${account.currency}</td>
                        <td>${account.blocked}</td>
                    </tr>
                </c:if>
            </c:forEach>
        </table>
    </div>
</body>
</html>
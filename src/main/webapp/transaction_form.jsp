<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/css/style.css" />
    <title>Transactions</title>
</head>
<body>
<div id="navigation_menu">
    <ul>
        <li><a href="/customers">Customers</a></li>
        <li><a href="/accounts">Accounts</a></li>
        <li><a id="current_page" href="/transactions">Transactions</a></li>
        <c:if test="${sessionScope.role == 'ADMIN'}">
            <li><a href="/users">Users</a></li>
        </c:if>
        <li id="home"><a href="/index"><img src="/images/home13.png" /></a></li>
    </ul>
</div>
<div id="main_content">
    <h1>Create Transaction</h1>
    <form id="transaction_form" action="">
        Amount<br/>
        <input id="amount" type="text" name="amount" placeholder="Amount" /><br />
        Operation type<br/>
        <select id="operationType" name="operationType">
            <option>PUT</option>
            <option>WSD</option>
        </select><br/>
        Account number<br/>
        <select id="accountNumber" name="accountNumber">
            <c:forEach var="account" items="${accountList}">
                <option>${account.accountNumber}</option>
            </c:forEach>
        </select>
        </br>
        <div id="transaction_save">Save</div>
    </form>
    <table id="transaction_list">
        <tr>
            <th>Amount</th>
            <th>Date</th>
            <th>Operation type</th>
            <th>Account number</th>
            <th></th>
        </tr>
        <c:forEach var="transaction" items="${transactionList}">
            <c:if test="${(sessionScope.role == 'ADMIN') || (sessionScope.userId == transaction.userId)}">
                <tr id="${transaction.id}">
                    <td>${transaction.amount}</td>
                    <td>${transaction.date}</td>
                    <td>${transaction.operationType}</td>
                    <td>${transaction.accountNumber}</td>
                    <td><div id="transaction_delete"><img src="/images/delete.png" /></div></td>
                </tr>
            </c:if>
        </c:forEach>
        </table>
    </div>
    <script type="text/javascript" src="/javascript/jquery.js"></script>
    <script type="text/javascript" src="/javascript/script.js"></script>
</body>
</html>
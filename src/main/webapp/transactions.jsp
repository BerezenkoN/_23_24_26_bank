<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/css/style.css" />
    <title>Transactions</title>
</head>
<body>
<div id="navigation_menu">
    <ul>
        <li><a href="/customers">Customers</a></li>
        <li><a href="/accounts">Accounts</a></li>
        <li><a id="current_page" href="/transactions">Transactions</a></li>
        <c:if test="${sessionScope.role == 'ADMIN'}">
            <li><a href="/users">Users</a></li>
        </c:if>
        <li id="home"><a href="/index"><img src="/images/Home13.png" /></a></li>
    </ul>
</div>
<div id="main_content">
    <h1>Transactions</h1>
    <a href="/transactions/new"><div id="create_transaction">Create transaction</div></a>
    <table id="transaction_list">
        <tr>
            <th>Amount</th>
            <th>Date</th>
            <th>Operation type</th>
            <th>Account number</th>
            <th></th>
        </tr>
        <c:forEach var="transaction" items="${transactionList}">
            <c:if test="${(sessionScope.role == 'ADMIN') || (sessionScope.userId == transaction.userId)}">
                <tr id=${transaction.id}>
                    <td>${transaction.amount}</td>
                    <td>${transaction.date}</td>
                    <td>${transaction.operationType}</td>
                    <td>${transaction.accountNumber}</td>
                    <td><div id="transaction_delete"><img src="/images/delete.png" /></div></td>
                </tr>
            </c:if>
        </c:forEach>
        </table>
    </div>
    <script type="text/javascript" src="/javascript/jquery.js"></script>
    <script type="text/javascript" src="/javascript/script.js"></script>
</body>
</html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/css/style.css" />
    <title>Users</title>
</head>
<body>
<div id="navigation_menu">
    <ul>
        <li><a href="/customers">Customers</a></li>
        <li><a href="/accounts">Accounts</a></li>
        <li><a href="/transactions">Transactions</a></li>
        <c:if test="${sessionScope.role == 'ADMIN'}">
            <li><a id="current_page" href="/users">Users</a></li>
        </c:if>
        <li id="home"><a href="/index"><img src="/images/Home13.png" /></a></li>
    </ul>
</div>
<div id="main_content">
    <h1>Create user</h1>
    <form id="user_form" action="">
            Login<br />
            <input id="username" type="text" name="username" placeholder="Login" /><br />
            Password<br />
            <input id="password" type="password" name="password" placeholder="Password" /><br />
            Name<br />
            <input id="name" type="text" name="name" placeholder="Name" /><br />
            User role<br />
            <select id="userRole" name="userRole">
                <option>ADMIN</option>
                <option>CUSTOMER</option>
            </select><br />
            Customer<br />
            <select id="customer" name="customer">
                <c:forEach var="customer" items="${customerList}">
                    <option value="${customer.id}">${customer.firstName} ${customer.lastName}</option>
                </c:forEach>
            </select></br>
            <div id="user_save">Save</div>
    </form>
    <table id="user_list">
        <tr>
            <th>Login</th>
            <th>Name</th>
            <th>Role</th>
            <th>Customer</th>
            <th></th>
        </tr>
        <c:forEach var="user" items="${userList}">
            <tr id="${user.id}">
                <td>${user.login}</td>
                <td>${user.name}</td>
                <td>${user.role}</td>
                <td>${user.customer}</td>
                <td><div id="user_delete"><img src="/images/delete.png" /></div></td>
            </tr>
        </c:forEach>
    </table>
</div>
    <script type="text/javascript" src="/javascript/jquery.js"></script>
    <script language="javascript" type="text/javascript" src="/javascript/script.js"></script>
</body>
</html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/css/style.css" />
    <title>Accounts</title>
</head>
<body>
    <div id="navigation_menu">
        <ul>
            <li><a href="/customers">Customers</a></li>
            <li><a id="current_page" href="/accounts">Accounts</a></li>
            <li><a href="/transactions">Transactions</a></li>
            <c:if test="${sessionScope.role == 'ADMIN'}">
                <li><a href="/users">Users</a></li>
            </c:if>
            <li id="home"><a href="/index"><img src="/images/Home13.png" /></a></li>
        </ul>
    </div>
    <div id="main_content">
        <h1>Accounts</h1>
        <a href="/accounts/new"><div id="create_account">Create account</div></a>
        <table id="account_list">
            <tr>
                <th>Account number</th>
                <th>Balance</th>
                <th>Creation date</th>
                <th>Currency</th>
                <th>Blocked</th>
                <th></th>
            </tr>
            <c:forEach var="account" items="${accountList}">
            <c:if test="${(sessionScope.role == 'ADMIN') || (sessionScope.userId == account.userId)}">
            <tr id="${account.accountNumber}">
                <td><a href="/accounts/${account.accountNumber}">${account.accountNumber}</a></td>
                <td>${account.balance}</td>
                <td>${account.creationDate}</td>
                <td>${account.currency}</td>
                <td>${account.blocked}</td>
                <td><div id="account_delete"><img src="/images/delete.png" /></div></td>
            </tr>
            </c:if>
            </c:forEach>
        </table>
    </div>
    <script type="text/javascript" src="/javascript/jquery.js"></script>
    <script type="text/javascript" src="/javascript/script.js"></script>
</body>
</html>